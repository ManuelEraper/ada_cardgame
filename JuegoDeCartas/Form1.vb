﻿Public Class Form1

    'Creacion de los oros
    Dim _1deOro As New Carta(1, "Oro", "1 de oro")
    Dim _2deOro As New Carta(2, "Oro", "2 de oro")
    Dim _3deOro As New Carta(3, "Oro", "3 de oro")
    Dim _4deOro As New Carta(4, "Oro", "4 de oro")
    Dim _5deOro As New Carta(5, "Oro", "5 de oro")
    Dim _6deOro As New Carta(6, "Oro", "6 de oro")
    Dim _7deOro As New Carta(7, "Oro", "7 de oro")
    Dim _8deOro As New Carta(8, "Oro", "8 de oro")
    Dim _9deOro As New Carta(9, "Oro", "9 de oro")
    Dim _10deOro As New Carta(10, "Oro", "10 de oro")
    Dim _11deOro As New Carta(11, "Oro", "11 de oro")
    Dim _12deOro As New Carta(12, "Oro", "12 de oro")
    'Creacion de las espadas
    Dim _1deEspadas As New Carta(1, "Espadas", "1 de espadas")
    Dim _2deEspadas As New Carta(2, "Espadas", "2 de espadas")
    Dim _3deEspadas As New Carta(3, "Espadas", "3 de espadas")
    Dim _4deEspadas As New Carta(4, "Espadas", "4 de espadas")
    Dim _5deEspadas As New Carta(5, "Espadas", "5 de espadas")
    Dim _6deEspadas As New Carta(6, "Espadas", "6 de espadas")
    Dim _7deEspadas As New Carta(7, "Espadas", "7 de espadas")
    Dim _8deEspadas As New Carta(8, "Espadas", "8 de espadas")
    Dim _9deEspadas As New Carta(9, "Espadas", "9 de espadas")
    Dim _10deEspadas As New Carta(10, "Espadas", "10 de espadas")
    Dim _11deEspadas As New Carta(11, "Espadas", "11 de espadas")
    Dim _12deEspadas As New Carta(12, "Espadas", "12 de espadas")
    'Creacion de las copas
    Dim _1deCopas As New Carta(1, "Copas", "1 de copas")
    Dim _2deCopas As New Carta(2, "Copas", "2 de copas")
    Dim _3deCopas As New Carta(3, "Copas", "3 de copas")
    Dim _4deCopas As New Carta(4, "Copas", "4 de copas")
    Dim _5deCopas As New Carta(5, "Copas", "5 de copas")
    Dim _6deCopas As New Carta(6, "Copas", "6 de copas")
    Dim _7deCopas As New Carta(7, "Copas", "7 de copas")
    Dim _8deCopas As New Carta(8, "Copas", "8 de copas")
    Dim _9deCopas As New Carta(9, "Copas", "9 de copas")
    Dim _10deCopas As New Carta(10, "Copas", "10 de copas")
    Dim _11deCopas As New Carta(11, "Copas", "11 de copas")
    Dim _12deCopas As New Carta(12, "Copas", "12 de copas")
    'Creacion de las Basto
    Dim _1deBasto As New Carta(1, "Basto", "1 de basto")
    Dim _2deBasto As New Carta(2, "Basto", "2 de basto")
    Dim _3deBasto As New Carta(3, "Basto", "3 de basto")
    Dim _4deBasto As New Carta(4, "Basto", "4 de basto")
    Dim _5deBasto As New Carta(5, "Basto", "5 de basto")
    Dim _6deBasto As New Carta(6, "Basto", "6 de basto")
    Dim _7deBasto As New Carta(7, "Basto", "7 de basto")
    Dim _8deBasto As New Carta(8, "Basto", "8 de basto")
    Dim _9deBasto As New Carta(9, "Basto", "9 de basto")
    Dim _10deBasto As New Carta(10, "Basto", "10 de basto")
    Dim _11deBasto As New Carta(11, "Basto", "11 de basto")
    Dim _12deBasto As New Carta(12, "Basto", "12 de basto")

    Dim cartas(46) As Carta

    Public Sub Render(carta As Carta, y As String, x As String)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()


        Me.PictureBox1.Image = RenderCarta(carta.Imagen)
        Me.PictureBox1.Location = New System.Drawing.Point(y, x)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(141, 197)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False

        Me.Controls.Add(Me.PictureBox1)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Function RenderCarta(imagen As String) As Image
        Select Case imagen
            Case "1 de oro"
                Return Global.JuegoDeCartas.My.Resources.Resources._1_de_oro

        End Select
    End Function
End Class
