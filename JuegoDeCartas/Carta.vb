﻿Public Class Carta
    Dim _numero As Integer
    Dim _palo As String
    Dim _imagen As String

    Public Property Numero As Integer
        Get
            Return Numero
        End Get
        Set(value As Integer)
            _numero = value
        End Set
    End Property

    Public Property Palo As String
        Get
            Return Palo
        End Get
        Set(value As String)
            _palo = value
        End Set
    End Property

    Public Property Imagen As String
        Get
            Return _imagen
        End Get
        Set(value As String)
            _imagen = value
        End Set
    End Property

    Public Sub New(numero As Integer, palo As String, imagen As String)
        _numero = numero
        _palo = palo
        _imagen = imagen
    End Sub
End Class
